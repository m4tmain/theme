<?php 
/*
Template Name: Pricing Plan
*/ 
?>
	<?php get_header(); ?>
	<style type="text/css">
/*--------- Font ------------*/
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);
@import url(https://mining4truth.com/wp-content/themes/bridge/css/weloveiconfonts.css);
/* fontawesome */

[class*="fontawesome-"]:before {
  font-family: 'FontAwesome', sans-serif;
}

* {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.q_logo a
{
	visibility: visible!important;
    height: 56px !important;
}
.wrapper
{
	    background: #F2F2F2;
}
.content {
    margin-top: 110px;
}
/*------ utiltity classes -----*/
.fl{ float:left; }
.fr{ float: right; }
/*its also known as clearfix*/
.group:before,
.group:after {
    content: "";
    display: table;
} 
.group:after {
    clear: both;
}
.group {
    zoom: 1;  /*For IE 6/7 (trigger hasLayout) */
}

body {
    background: #F2F2F2;
    font-family: 'Droid Sans', sans-serif;
    line-height: 1;
    font-size: 16px;    
}
.wrapper {
    
}
.pricing-table {
    width: 80%;
    margin: 50px auto;
    text-align: center;
    padding: 10px;
    padding-right: 0;
}
.pricing-table .heading{
    color: #9C9E9F;
    text-transform: uppercase;
    font-size: 1.3rem;
    margin-bottom: 4rem;
}
.block{
    width: 30%;    
    margin: 0 15px;
    overflow: hidden;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;    
/*    border: 1px solid red;*/
}
/*Shared properties*/
.title,.pt-footer{
    color: #FEFEFE;
    text-transform: capitalize;
    line-height: 2.5;
    position: relative !important;
    font-size: 18px;
    font-weight: normal;
    letter-spacing: normal;
	    width: auto;
    height: auto;
    text-align: center;
    z-index: auto;
}
.desc{
    position: relative;
    color: #FEFEFE;
    padding: 55px 0 10px 0;
}
/*arrow creation*/
.desc:after, .desc:before,.pt-footer:before,.pt-footer:after {
	top: 100%;
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}
.pt-footer:after,.pt-footer:before{
    top:0;
}
.desc:after,.pt-footer:after {
	border-color: rgba(136, 183, 213, 0);	
	border-width: 5px;
	margin-left: -5px;
}
/*/arrow creation*/
.price{
    position: relative;
    display: inline-block;
    margin-bottom: 0.625rem;
	    color: white;
}
.price span{    
    font-size: 6rem;
    letter-spacing: 8px;
    font-weight: bold;        
}
.price sup{
    font-size: 1.5rem;    
    position: absolute;    
    top: 12px;
    left: -12px;
}
.hint{
    font-style: italic;
    font-size: 0.9rem;
}
.features{
    list-style-type: none;    
    background: #FFFFFF;
    text-align: left;
    color: #9C9C9C;
    padding:30px 15%;
    font-size: 0.9rem;
	min-height: 30px;
}
.features li{
    padding:15px 0;
    width: 100%;
}
.features li span{
   padding-right: 0.4rem; 
}
.pt-footer a{
	color: white;
    font-size: 16px;
    font-weight: bold;
}	
.pt-footer{
    font-size: 0.95rem;
    text-transform: capitalize;
	padding: 10px 0px;
}
/*PERSONAL*/
.personal .title{        
    background: #78CFBF;    
}
.personal .desc,.personal .pt-footer{
    background: #82DACA;
}
.personal .desc:after{	
	border-top-color: #82DACA;	
}
.personal .pt-footer:after{
    border-top-color: #FFFFFF;
}
/*PROFESSIONAL*/
.professional .title{
    background: #3EC6E0;
}
.professional .desc,.professional .pt-footer{
    background: #53CFE9;
}
.professional .desc:after{	
	border-top-color: #53CFE9;	
}
.professional .pt-footer:after{
    border-top-color: #FFFFFF;
}
/*BUSINESS*/
.business .title{
    background: #E3536C;
}
.business .desc,.business .pt-footer{
    background: #EB6379;
}
.business .desc:after{	
	border-top-color: #EB6379;	
}
.business .pt-footer:after {	
	border-top-color: #FFFFFF;	
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>


jQuery.noConflict();
	(function( $ ) {

		$(document).ready(function(){
								   
		   var basicprice = 1500;
		   var saving = '0000';
								   
			$("#basicdiv").click(function(){
				
				showamount('academy-basic',basicprice,saving,'academy','basic','1683'); // for accademy basic
			});
								   
			$("#goldbtn").click(function(){
				$("#goldpanel").slideToggle();
			});
			
			$("#goldplusbtn").click(function(){
				$("#goldpluspanel").slideToggle();
			});
			
			
			$(".pricespan").click(function(){ // for accademy gold and accademy gold plus
				
				var tempprice = $( this ).attr( "price" );
				tempprice = +tempprice + +basicprice;
				var tempsaving = $( this ).attr( "saving" );
				var hidden_value = $( this ).attr( "name" );
				var packagename = $( this ).attr( "package" );
				var variation_id = $( this ).attr( "variation_id" );
				var option_value = tempprice+'-'+tempsaving;
				$("#academy_options_value").val(option_value);
				var hidden_field = "academy"; 
				//alert(variation_id);
				if(packagename == 'academy_gold')
				{
					$("#goldpanel").slideToggle();
				}
				else if(packagename == 'academy_gold_plus')
				{
					$("#goldpluspanel").slideToggle();
				}
				
				
				showamount(packagename,tempprice,tempsaving,hidden_field,hidden_value,variation_id); 
				
				
			});
			$(".packages_selectbox").change(function(){ /// for all other select boxes
													 //alert(1);
				var selectboxid = $( this ).attr( "id" );
				//var vid = $( this ).attr( "variation_id" ); alert(vid);
				var selectbox = "#"+selectboxid;
				var temp = $(selectbox).val();
				if(temp != ''){
					var res = temp.split("-");
					//alert(selectbox);
					var hidden_field = selectboxid;
					var hidden_value = temp;
					showamount(res[0],res[1],res[2],hidden_field,hidden_value,res[3]);
				}
				else
				{
					var current_amountdiv = "#"+selectboxid+"_savingdiv";
					 var disp = $(current_amountdiv).css("display");
					 //alert(disp);
					 if(disp == 'block'){$(current_amountdiv).slideToggle();}
				}
				
			
				
			});
			
			
			
			/// shows amount and saving - update hidden field and  cart button link
			function showamount(package_option,price,saving,hidden_field,hidden_value,variation_id)
			{
			
				
				var current_hidden_field = "#"+hidden_field+"_options";
				var current_pricediv = "#"+hidden_field+"_pricediv";
				var current_savingdiv = "#"+hidden_field+"_savingdiv";
				var current_amountdiv = "#"+hidden_field+"_savingdiv";
				//alert(current_pricediv+'--'+current_savingdiv);
				$(current_hidden_field).val(hidden_value);
				
				
				///update price and saving
				
				$( current_pricediv ).html('<sup>$</sup><span>'+price+'</span><sub>/mo.</sub>');
				$( current_savingdiv ).html( "You Saved $"+saving+"" );
				
				
				 var disp = $(current_amountdiv).css("display");
				 //alert(disp);
				 if(disp == 'none'){$(current_amountdiv).slideToggle();}
				 
				//$( "#amountdiv").slideDown('slow');
				
				
				/// update cart button link
				
				var product_id = '';
				if(hidden_field == 'gold')
				 product_id = 1179;
				 else if(hidden_field == 'gold_unlimited')
				 product_id = 1180;
				 else if(hidden_field == 'individual_trade')
				 product_id = 1182;
				  else if(hidden_field == 'academy')
				 product_id = 1181;
				
				var package_btn_id = "#"+hidden_field+"_package_button";
			
				var buttonlink = "https://mining4truth.com/cart/?add-to-cart="+product_id+"&variation_id="+variation_id+"";
				
				
				$(package_btn_id).attr("href", buttonlink);
				//package_button
				
				//alert(package_btn_id);
				
			}
		});

})(jQuery);
</script>

<style>
.accordion {
    background-color: white;
    color: #9C9C9C;
    cursor: pointer;
    padding: 18px 18px 18px 0;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    /*font-size: 15px;*/
    transition: 0.4s;
}

.active, .accordion:hover {
   color:black; 
}

.panel {
    /*padding: 0 18px;*/
    display: none;
    background-color: #eee;
}
.amountdiv {
	/*display:none;*/
}
.savingdiv,.pricediv
{
	width:100px;
	display:inline-block;
	margin:10px;
	padding:5px;
	text-align:center;
	border-radius:2px;
}

.savingdiv
{
	background:#4CAF50;
	color:white;
	margin-left:60px;
}
.savingdiv:hover
{
	background:white;
	color:#4CAF50;
}
.pricediv
{
	/*background:#008CBA;
	background:black;
	color:white;
	*/
	width:150px;
}
.pricediv:hover
{
	/*background:white;
	background:black;
	color:#008CBA;*/
}

.pricespan
{
	display:block;
	padding:15px;
	cursor:pointer;
}
.pricespan:hover
{
	background:#ccc;
	color:white;
}

.packages_selectbox
{
	background:white !important;
	color: #9C9C9C;
	margin-bottom:10px;
	border: 1px solid #eee;
    padding: 10px;
    border-radius: 5px;
}

.pricediv .price
{
	 font-size: 60px;
    line-height: 60px;
    padding: 0 3px;
    font-weight: 700;
    vertical-align: middle;
    display: inline-block;
    color: #090909;
}

.icon-li icon-angle-down
{
	float:right;
}

.price_table_inner
{
	background: white;
    min-height: 400px;
    margin-bottom: 10px;
    padding-top: 30px;
    /* padding: 10px; */
    border-radius: 21px;
}
.qbutton 
{
	background-color: #dcac34;
    border-radius: 5px;
	 margin: 0 auto !important;
    display: table !important;
}
</style>
	<div class="wrapper" style="background: #F2F2F2;">
        <!-- PRICING-TABLE CONTAINER -->
        <div class="pricing-table group first-row">
            <h1 class="heading">
                Pricing overview
            </h1>
            <!-- free -->
            <div class="block personal fl">
                <h2 class="title">FREE</h2>
                <!-- CONTENT -->
                <div class="desc">
                    <p class="price">
                        <sup>$</sup>
                        <span>0</span>
                        <sub>/mo.</sub>
                    </p>
                    <p class="hint" style="font-weight: normal;background: black;color: #fbd0d0;font-size: 16px;padding: 4px 0;">You Saved 0000</p>
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>1 WordPress Install</li>
                    <li><span class="fontawesome-star"></span>25,000 visits/mo.</li>
                    <li><span class="fontawesome-dashboard"></span>Unlimited Data Transfer</li>
                    <li><span class="fontawesome-cloud"></span>10GB Local Storage</li>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer">
                    <a id="free_package_button" href="https://mining4truth.com/cart/?add-to-cart=1748">Add to Cart</a>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /free -->
            
            
            
              <!-- CRYPTO ACADEMY -->
            <div class="block professional fl">
                <h2 class="title">ACADEMY</h2>
                <!-- description -->
                <div class="desc">
                    <p class="price" id="academy_pricediv">
                        <sup>$</sup>
                        <span>1500</span>
                        <sub>/mo.</sub>
                    </p>
                    <p class="hint amountdiv" id="academy_savingdiv" style="font-weight: normal;background: black;color: #fbd0d0;font-size: 16px;padding: 4px 0;">You Saved 0000</p>
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
                                                      
                <!--- for Accademy Box :  -->
                
                <form action="#" name="academy_package_form" id="academy_package_form" class="package_form" method="post">
                <div class="main_package_div">
                
                
                <div  class="accordion" id="basicdiv"><span class="fontawesome-circle-arrow-right"></span> Academy only $1500</div>
                
                
                <button type="button" class="accordion" id="goldbtn"><span class="fontawesome-circle-arrow-right"></span> Basic  gold trade &nbsp;<span class="fontawesome-angle-down"></span></button>
                <div class="panel" id="goldpanel">
                
                <span class="pricespan" price="199" saving="100" name="gold_monthly" package="academy_gold" variation_id="1684"><span class="fontawesome-arrow-right"></span> Monthly $199 + $1500</span>
                <span class="pricespan" price="747" saving="250" name="gold_quarterly" package="academy_gold" variation_id="1685"><span class="fontawesome-arrow-right"></span> Quarterly $747 + $1500</span>
                <span class="pricespan" price="1194" saving="700" name="gold_halfyear" package="academy_gold" variation_id="1686"><span class="fontawesome-arrow-right"></span> Half year $1194 + $1500</span>
                <span class="pricespan" price="1788" saving="1600" name="gold_fullyear" package="academy_gold" variation_id="1687"><span class="fontawesome-arrow-right"></span>   Full year $1788 + $1500</span>
                
                </div>
                
                
                <button type="button" class="accordion" id="goldplusbtn"><span class="fontawesome-circle-arrow-right"></span> Gold unlimited trade &nbsp;<span class="fontawesome-angle-down"></span></button>
                <div class="panel" id="goldpluspanel">
                
                <span class="pricespan" price="549" saving="150" name="goldplus_monthly" package="academy_gold_plus"  variation_id="1688"><span class="fontawesome-arrow-right"></span> Monthly $549 + $1500</span>
                <span class="pricespan" price="1947" saving="300" name="goldplus_quarterly" package="academy_gold_plus"  variation_id="1689"><span class="fontawesome-arrow-right"></span> Quarterly $1947 + $1500</span>
                <span class="pricespan" price="3594" saving="750" name="goldplus_half_year" package="academy_gold_plus"  variation_id="1690"><span class="fontawesome-arrow-right"></span> Half year $3594 + $1500</span> 
                <span class="pricespan" price="6708" saving="1650" name="goldplus_yearly" package="academy_gold_plus"  variation_id="1691"><span class="fontawesome-arrow-right"></span> Full year $6708 + $1500</span>
                </div>
                
                
                <input type="hidden" name="academy_options" id="academy_options" value="" />
                <input type="hidden" name="academy_options_value" id="academy_options_value" value="" />
                
                
                </div>
                </form>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer">
                    <a id="academy_package_button" href="https://mining4truth.com/cart/?add-to-cart=1181&variation_id=1683">Add to Cart</a>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /CRYPTO ACADEMY -->
            
            
            <!-- Silver -->
            <div class="block professional fl">
                <h2 class="title">Silver</h2>
                <!-- description -->
                <div class="desc">
                    <p class="price">
                        <sup>$</sup>
                        <span>22</span>
                        <sub>/mo.</sub>
                    </p>
                    <p class="hint" style="font-weight: normal;background: black;color: #fbd0d0;font-size: 16px;padding: 4px 0;">You Saved 0000</p>
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>10 WordPress Install</li>
                    <li><span class="fontawesome-star"></span>100,000 visits/mo.</li>
                    <li><span class="fontawesome-dashboard"></span>Unlimited Data Transfer</li>
                    <li><span class="fontawesome-cloud"></span>20GB Local Storage</li>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer">
                   <a href="https://mining4truth.com/cart/?add-to-cart=1168">Add to Cart</a>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /Silver -->
          
        </div>
        <!-- /PRICING-TABLE -->
		<div class="pricing-table group second-row">
        
           <!-- INDIVIDUAL TRADE -->
            <div class="block business fl">
                <h2 class="title">INDIVIDUAL TRADE</h2>
                <!-- CONTENT -->
                <div class="desc">
                    <p class="price" id="individual_trade_pricediv">
                        <sup>$</sup>
                        <span>99</span>
                        <sub>/mo.</sub>
                    </p>
                    <p id="individual_trade_savingdiv" class="hint  amountdiv" style="font-weight: normal;background: black;color: #fbd0d0;font-size: 16px;padding: 4px 0;">You Saved 0000</p>
                </div>
                <!-- /CONTENT -->

                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>25 WordPress Install</li>
                    <li><span class="fontawesome-star"></span>400,000 visits/mo.</li>
                    <li><span class="fontawesome-dashboard"></span>Unlimited Data Transfer</li>
                    
                 <!--- for Individual trade recommendation :  -->
                    <form action="#" name="individual_trade_form" id="individual_trade_form"  class="package_form" method="post"><div  class="main_package_div">
                    
                    <select name="individual_trade" id="individual_trade" class="packages_selectbox">
                    
                    <option value="1-99-0-1694">$ 99 USD </option>
                    <option value="2-189-10-1695">2 for $ 189 USD</option>
                    <option  value="3-277-20-1696">3 for $ 277 USD</option>
                    <option value="4-366-30-1697">4 for $ 366 USD </option>
                    <option value="5-455-40-1698">5 for $ 455 USD</option>
                    <option value="6-544-50-1699">6 for $ 544 USD</option>
                    <option value="7-633-60-1700">7 for $ 633 USD</option>
                    <option value="8-717-75-1701">8 for $ 717 USD</option>
                    <option value="9-808-83-1702">9 for $ 808 USD</option>
                    <option value="10-888-102-1703">10 for $ 888 USD</option>
                    </select>
                   
                    
                   
                    <input type="hidden" name="individual_trade_options" id="individual_trade_options" value="" />
                    </div></form>
                    
                    
                </ul>
                <!-- /FEATURES -->

                <!-- PT-FOOTER -->
                <div class="pt-footer">
                   <a id="individual_trade_package_button" href="https://mining4truth.com/cart/?add-to-cart=1182&variation_id=1694">Add to Cart</a>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /INDIVIDUAL TRADE -->
        
        
          <!-- GOLD -->
            <div class="block business fl">
                <h2 class="title">GOLD</h2>
                <!-- CONTENT -->
                <div class="desc">
                    <p class="price" id="gold_pricediv" >
                        <sup>$</sup>
                        <span>299</span>
                        <sub>/mo.</sub>
                    </p>
                    <p class="hint amountdiv" id="gold_savingdiv" style="font-weight: normal;background: black;color: #fbd0d0;font-size: 16px;padding: 4px 0;">You Saved 0000</p>
                </div>
                <!-- /CONTENT -->

                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>25 WordPress Install</li>
                    <li><span class="fontawesome-star"></span>400,000 visits/mo.</li>
                    <li><span class="fontawesome-dashboard"></span>Unlimited Data Transfer</li>
                   
                    <!--- for Gold membership  :  -->
                    <form action="#" name="gold_form" id="gold_form"  class="package_form" method="post"><div  class="main_package_div">
                    
                    <select name="gold" id="gold" class="packages_selectbox">
                    
                    <option value="monthly-299-0-1704">Monthly $299 </option>
                    <option value="quarterly-747-150-1705">Quarterly $747</option>
                    <option  value="half_year-1194-600-1706">Half year $1194</option>
                    <option value="yearly-1788-1500-1707"> Yearly $1788</option>
                    
                    </select>
                    
                    <input type="hidden" name="gold_options" id="gold_options" value="" />
                    
                    </div></form>
                </ul>
                <!-- /FEATURES -->

                <!-- PT-FOOTER -->
                <div class="pt-footer">
                  <a id="gold_package_button" href="https://mining4truth.com/cart/?add-to-cart=1182&variation_id=1704">Add to Cart</a>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /GOLD -->
            
            
            <!-- GOLD UNLIMITED -->
            <div class="block personal fl">
                <h2 class="title">GOLD UNLIMITED</h2>
                <!-- CONTENT -->
                <div class="desc">
                    <p class="price"  id="gold_unlimited_pricediv">
                        <sup>$</sup>
                        <span>799</span>
                        <sub>/mo.</sub>
                    </p>
                    <p class="hint amountdiv" id="gold_unlimited_savingdiv" style="font-weight: normal;background: black;color: #fbd0d0;font-size: 16px;padding: 4px 0;">You Saved 0000</p>
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>1 WordPress Install</li>
                    <li><span class="fontawesome-star"></span>25,000 visits/mo.</li>
                    <li><span class="fontawesome-dashboard"></span>Unlimited Data Transfer</li>
                    
                    
                                    
                <!--- for Gold Unlimited :  -->
                <form action="#" name="gold_unlimited_form" id="gold_unlimited_form"  class="package_form" method="post"><div  class="main_package_div">
                
                <select name="gold_unlimited" id="gold_unlimited" class="packages_selectbox">
                
                <option value="monthly-799-0-1708" variation_id = "1241">Monthly $799  </option>
                <option value="quarterly-2247-150-1709">Quarterly $2247 </option>
                <option  value="half_year-4194-600-1710">Half year $4194</option>
                <option value="yearly-7788-1500-1711">Yearly $7788</option>
                </select>
                
                <input type="hidden" name="gold_unlimited_options" id="gold_unlimited_options" value="" />
                </div></form>


                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer">
                   <a id="gold_unlimited_package_button" href="https://mining4truth.com/cart/?add-to-cart=1168&variation_id=1708">Add to Cart</a>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /GOLD UNLIMITED -->
          
         
        </div>
        <!-- /PRICING-TABLE -->
    </div>
	
				
	<?php do_action('qodef_page_after_container') ?>
	<?php get_footer(); ?>