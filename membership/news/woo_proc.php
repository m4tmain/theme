<?php
require_once("wp-load.php");

if(isset($_POST["billing_first_name"])) {

	$email = $_POST["billing_email"];
	$user = preg_replace('~@.*$~', "", $email);

	$billing_user = $_POST["billing_first_name"];
	$billing_last_name = $_POST["billing_last_name"];
	$billing_company = $_POST["billing_company"];
	$billing_country = $_POST["billing_country"];
	$billing_address_1 = $_POST["billing_address_1"];
	$billing_address_2 = $_POST["billing_address_2"];
	$billing_city = $_POST["billing_city"];
	$billing_state = $_POST["billing_state"];
	$billing_postcode = $_POST["billing_postcode"];
	$billing_phone = $_POST["billing_phone"];
	$billing_email = $_POST["billing_email"];
	$account_password = $_POST["account_password"];
	$shipping_first_name = $_POST["shipping_first_name"];
	$shipping_last_name = $_POST["shipping_last_name"];
	$shipping_company = $_POST["shipping_company"];
	$shipping_country = $_POST["shipping_country"];
	$shipping_address_1 = $_POST["shipping_address_1"];
	$shipping_address_2 = $_POST["shipping_address_2"];
	$shipping_city = $_POST["shipping_city"];
	$shipping_state = $_POST["shipping_state"];
	$shipping_postcode = $_POST["shipping_postcode"];
	$order_comments = $_POST["order_comments"];

	$user_id = wc_create_new_customer($email, $user, $account_password);

	update_user_meta($user_id, "billing_first_name", $billing_user);
	update_user_meta($user_id, "billing_last_name", $billing_last_name);
	update_user_meta($user_id, "billing_company", $billing_company);
	update_user_meta($user_id, "billing_country", $billing_country);
	update_user_meta($user_id, "billing_address_1", $billing_address_1);
	update_user_meta($user_id, "billing_address_2", $billing_address_2);
	update_user_meta($user_id, "billing_city", $billing_city);
	update_user_meta($user_id, "billing_state", $billing_state);
	update_user_meta($user_id, "billing_postcode", $billing_postcode);
	update_user_meta($user_id, "billing_phone", $billing_phone);
	update_user_meta($user_id, "billing_email", $billing_email);
	update_user_meta($user_id, "account_password", $account_password);
	update_user_meta($user_id, "shipping_first_name", $shipping_first_name);
	update_user_meta($user_id, "shipping_last_name", $shipping_last_name);
	update_user_meta($user_id, "shipping_company", $shipping_company);
	update_user_meta($user_id, "shipping_country", $shipping_country);
	update_user_meta($user_id, "shipping_address_1", $shipping_address_1);
	update_user_meta($user_id, "shipping_address_2", $shipping_address_2);
	update_user_meta($user_id, "shipping_city", $shipping_city);
	update_user_meta($user_id, "shipping_state", $shipping_state);
	update_user_meta($user_id, "shipping_postcode", $shipping_postcode);
	update_user_meta($user_id, "order_comments", $order_comments);
}

if(isset($_POST["register"])) {
	$user = $_POST["username"];
	$first_name = $_POST["first_name"];
	$last_name = $_POST["last_name"];
	$nick = $_POST["nickname"];
	$email = $_POST["email"];
	$website = $_POST["website"];
	$desc = $_POST["description"];
	$pass = $_POST["passw1"];

	$user_id = wc_create_new_customer($email, $user, $pass);

}
