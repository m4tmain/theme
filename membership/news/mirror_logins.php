<?php

require_once("wp-load.php");

if(isset($_GET["user"])){
	$user_login = $_GET["user"];
	$user = get_user_by("login", $user_login);
	if(!is_wp_error($user)){
		wp_clear_auth_cookie();
		wp_set_current_user($user->ID);
		wp_set_auth_cookie($user->ID);
	
		$url = "http://mining4truth.com/my-account/";
		wp_redirect($url);
		exit;
	}
}

if(isset($_GET["logout"]) && $_GET["logout"] == 1) {
	wp_logout();
	$url = "https://mining4truth.com/?mlogout=1&route=final";
	wp_redirect($url);
	exit;
}
